<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/settings', 'HomeController@settings')->name('settings');
Route::post('/save_url', 'HomeController@getUrl');

Route::get('login/google', 'Auth\RegisterController@redirectToProvider');
Route::get('login/google/callback', 'Auth\RegisterController@handleProviderCallback');
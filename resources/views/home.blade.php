@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Sites!!</div>

                <div class="panel-body">

                     <span style="color: red">

                          @if($errors->any())
                             <h4>{{$errors->first()}}</h4>
                         @endif
                     </span>
                    <form method="post" action="{{url('/save_url')}}">
                        {{csrf_field()}}
                        <input type="text" name="getUrl" id="getUrl" class="urlinput">
                        <button type="submit">Get Code</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

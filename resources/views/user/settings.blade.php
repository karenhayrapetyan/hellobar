@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Settings!!</div>

                <div class="panel-body">
                    @if(Session::has('msg'))
                        <div class="alert alert-info">
                            <a class="close" data-dismiss="alert">×</a>
                            {!!Session::get('msg')!!}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

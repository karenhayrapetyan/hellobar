<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Site;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session as Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    function IsUrl($url){

        $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
        $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
        $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
        $regex .= "(\:[0-9]{2,5})?"; // Port
        $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
        $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
        $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

        if(preg_match("/^$regex$/i", $url)) // `i` flag for case-insensitive
        {
            return true;
        }
    }

    public function getUrl(Request $request)
    {

        $user_url = $request->getUrl;
        $user_id = Auth::user()->id;

        if($this->IsUrl($user_url)){
            Site::create([
                'user_id' => $user_id,
                'user_url' =>$user_url,
            ]);

            Session::flash('msg','Your Url  added.');
            return redirect()->route('settings');

        }else{

            $msg = 'Its Not Url';

            return Redirect::back()->withErrors([$msg, 'Message']);
        }


    }

    public function settings()
    {

        return view('user/settings');
    }
}
